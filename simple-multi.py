#!/usr/bin/env python3

import asyncio
import websockets

clients = {}

async def hello(websocket, path):
    print('New client', websocket)
    print('({} existing clients)'.format(len(clients)))

    # The first line from the client is the name
    name = await websocket.recv()
    await websocket.send('Welcome to websocket-chat, {}'.format(name))
    await websocket.send('There are {} other users connected: {}'.format(len(clients), list(clients.values())))
    clients[websocket] = name
    for client, _ in clients.items():
        await client.send(name + ' has joined the chat')

    # Handle messages from this client
    while True:
        print("toto")
        try:
            message = await websocket.recv()
        except websockets.exceptions.ConnectionClosed:
            del clients[websocket]
            print('Client closed connection', websocket)
            for client, _ in clients.items():
                await client.send('Client has left the chat')
            break

        # Send message to all clients
        for client, _ in clients.items():
            print("titi")
            await client.send('{}: {}'.format(name, message))


start_server = websockets.serve(hello, 'localhost', 8765)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()

