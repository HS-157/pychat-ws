#!/usr/bin/env python3

import asyncio
import websockets
import json

clients = {}

c = {
    "name": "Toto",
    "msg": "Hello world",
    "type": "BEGIN"
}

a = [
    {'name': 'Toto', 'msg': 'Hello', 'type': 'BEGIN'},
    {'name': 'Toto', 'msg': 'Wolrd', 'type': 'MSG'},
    {'name': 'Titi', 'msg': 'Quit()', 'type': 'END'},
    {'name': 'Titi', 'msg': 'Huhu', 'type': 'BEGIN'},
    {'name': 'Titi', 'msg': 'le monde', 'type': 'MSG'},
    {'name': 'Titi', 'msg': 'Bye bye', 'type': 'END'}
]

for i in a:
    print(json.dumps(i))

async def send(msg):
    encode = json.dumps(msg)
    for client, _ in clients.items():
        await client.send(json.dumps(msg))


async def hello(websocket, path):

    clients[websocket] = None

    while websocket in clients:
        try:
            msg = json.loads(await websocket.recv())
        except websockets.exceptions.ConnectionClosed:
            del clients[websocket]
            print("Client quit (except) : %s" % msg.get("name"))
            r = {
                "name": "Server",
                "msg": "Welcome to channel : %s" % msg.get("name"),
                "type": "SRV"
                }
            await send(r)
            # break
        except json.decoder.JSONDecodeError:
            await websocket.send("Please JSON !")
            await hello(websocket, path)
            break

        if msg.get("type") == "BEGIN" and not websocket in clients:
            r = {
                "name": "Server",
                "msg": "Welcome to channel : %s" % msg.get("name"),
                "type": "SRV"
            }
            await websocket.send(r)

            clients[websocket] = msg.get("name")
            join = {
                "name": "Server",
                "msg": "%s has joined the chat" % msg.get("name"),
                "type": "SRV"
            }
            print("%s : %s" % (join.get("name"), join.get("msg")))
            await send(join)

        if msg.get("type") == "END":
            websocket.close()
            del clients[websocket]
            print("Client quit (msg) : %s" % msg.get("name"))
            break

        print("%s : %s" % (msg.get("name"), msg.get("msg")))

        await send(msg)

start_server = websockets.serve(hello, 'localhost', 8765)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
